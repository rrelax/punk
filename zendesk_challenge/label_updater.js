// ==UserScript==
// @name         Punk Challenge Script
// @namespace    http://gitlab.com/rrelax
// @version      0.1
// @description  Address the challenges listed on rverschoor/punk
// @author       @rrelax
// @match        https://gitlab.zendesk.com/agent/tickets/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=zendesk.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Utility functions
    function isNullOrUndefined(item){
        return item == null || item == undefined;
    }

    // Main code body
    var labelElements = document.getElementsByTagName("label");
    var regionLabel = null;

    var regionLabelSearchString = "Preferred Region for Support";

    for(let element of labelElements){
        if(element.innerHTML == regionLabelSearchString) {
            regionLabel = element;
        }
    };

    if(!isNullOrUndefined(regionLabel)){
        regionLabel.innerHTML = "Region";
    } else {
        console.log("[!]\tCould not find the label element to fix 'region'");
    }

        // Swap out dropdown item labels
    //Need: parent of label, then next sibling element
    //Issue: It appears that the labels are stored in span elements - it's unclear how this is passed to the backend, implication is that the entire region string is passed.
    //Next step: Trial changing these elements and see if modifications to the amended dropdown are parsed and stored correctly.

})();
