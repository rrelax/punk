# Userscript repository

#### Be your own workspace anarchist!

## How to use

These scripts are meant to be used with a userscript manager like [Violentmonkey](https://violentmonkey.github.io).

## Compatibility

Violentmonkey is available for Chrome, Firefox, Edge, Chromium, Opera, Vivaldi, etc. \
The userscripts might be compatible with other userscript managers like [Tampermonkey](https://www.tampermonkey.net).

I've only used these scripts with [Violentmonkey](https://violentmonkey.github.io) and [Vivaldi](https://vivaldi.com), so you might run into incompatibility issues (yay browser API compatibility!). \
MRs welcome.

## Challenge

I had some nice ideas to improve Zendesk views to match my personal preference. \
E.g. abbreviate the `Preferred Region for Support` table header to just `Region`, and then abbreviate the cells to `AMER`, `APAC`, `EMEA`, and `All`. \
Technically-wise, that's easy to achieve in a user script. \
After numerous hours of cosing I gave up. \
The way Zendesk dynamically builds it pages conflicts with user scripts, and leads to erratic results. \
I would love to be proven wrong, and shown a way that achieves table modifications in a stable way!

## Scripts

Zendesk:
- [Zendesk: hide signature](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_signature)
- [Zendesk: active ticket tab](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_active_ticket_tab)
- [Zendesk: SLA colors](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_sla_colors)
- [Zendesk: trim user info](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_user_info) (Katrin Leinweber)
- [Zendesk: hayfever](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hayfever)
- [Zendesk: short SLA](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_short_sla)
- [Zendesk: trim NEEDS ORG reminders](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_needs_org) (Katrin Leinweber)
- [Zendesk: trim org note](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_org_note) (Katrin Leinweber)
- [Zendesk: trim HIGH prio reminder](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_prio_reminder) (Katrin Leinweber)
- [Zendesk: align status & SLA](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_align_status_sla)
- [Zendesk: hide pending reminders](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_pending_reminders)

Other:
- [CustomersDot: hayfever](https://gitlab.com/rverschoor/punk/-/tree/main/customersdot_hayfever)
- [LicenseDot: hayfever](https://gitlab.com/rverschoor/punk/-/tree/main/licensedot_hayfever)
- [SFDC: hayfever](https://gitlab.com/rverschoor/punk/-/tree/main/sfdc_hayfever)
- [Zuora: hayfever](https://gitlab.com/rverschoor/punk/-/tree/main/zuora_hayfever)
- [slic](https://gitlab.com/rverschoor/punk/-/tree/main/slic)
- [slic: duplicate license](https://gitlab.com/rverschoor/punk/-/tree/main/slic_duplicate) deprecated
