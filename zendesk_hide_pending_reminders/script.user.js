// ==UserScript==
// @name          Zendesk hide pending reminders
// @version       1.0.1
// @author        Rene Verschoor
// @description   Zendesk: hide automatic reminders on pending tickets
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_pending_reminders
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_pending_reminders/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_pending_reminders/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_pending_reminders/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const DEBUG = false;

/*
First reminder:
SupportOps Feb 18 23:05 (assign)
Hello,
This is a follow up for your ticket (#000000) with GitLab. We didn't hear from your for a few days. Perhaps you missed our last message?
Please reply if you'd still like help and we'll be happy to assist.

Second reminder:
SupportOps Feb 18 23:09 (assign)
Hello,
This is a 2nd follow up for your ticket (#000000) with GitLab. Perhaps you missed our previous messages?
To make sure everything's OK, we'll send an email asking for your feedback tomorrow.
Please reply if you'd still like help and we'll be happy to assist.
*/

(function() {

  // Watch for any change on the Zendesk page
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    po();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  // Find the reminder comments
  function po() {
    let comments_selector = '[data-test-id="ticket-audit-comment"]';
    let comments = document.querySelectorAll(comments_selector);
    comments.forEach((comment, index) => {
      if (comment.querySelector('div.content .header .actor .name').textContent.trim() === 'GitLab SupportOps Bot') {
        let contents = comment.querySelectorAll('div.content .body .comment .zd-comment p');
        if (contents.length >= 3) {
          if (contents[0].textContent === 'Hello,') {
            if (contents[1].textContent.startsWith('This is a follow up for your ticket') ||
                contents[1].textContent.startsWith('This is a 2nd follow up for your ticket')) {
              go(comments, index);
            }
          }
        }
      }
    })
  }

  // Remove elements
  function go(elements, index) {
    if (DEBUG) {
      elements[index].style.setProperty('background-color', 'hotpink', 'important')
    } else {
      elements[index].style.display = 'none';
    }
  }

})();
