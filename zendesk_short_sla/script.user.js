// ==UserScript==
// @name          Zendesk short SLA
// @version       1.0.0
// @author        Rene Verschoor
// @description   Zendesk: shorten SLA header
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_short_sla
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_short_sla/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_short_sla/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_short_sla/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const MINIMUM_NR_COLUMS = 4;
const SELECTOR_HEADER_TOP = '[data-test-id="table_header"] > thead > tr > th';
const SELECTOR_HEADER_SUB = '[data-test-id="table_main"] > thead > tr > th';
const TITLE_OLD = 'Next SLA breach';
const TITLE_NEW = 'SLA';

  (function() {

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver( mutations => {
    po(mutations);
  });
  observer.observe(document.body, { childList: true, subtree: true });

  function po(mutations) {
    let header = [];
    let subheader = [];
    let ths = document.querySelectorAll(SELECTOR_HEADER_TOP);
    let subths = document.querySelectorAll(SELECTOR_HEADER_SUB);
    // If table is being built, don't analyze it yet. Only act if there's a minimum # of columns
    if (ths.length >= MINIMUM_NR_COLUMS && subths.length >= MINIMUM_NR_COLUMS) {
      ths.forEach((th, index) => {
        header[index] = th.textContent;
      })
      subths.forEach((subth, index) => {
        subheader[index] = subth.textContent;
      })
      if (header.length === subheader.length && header.every((value, index) => value === subheader[index])) {
        const index = header.indexOf(TITLE_OLD);
        if (index >= 0) {
          ths[index].textContent = TITLE_NEW;
          subths[index].textContent = TITLE_NEW;
        }
      }
    }
  }

})();
