# Zendesk short SLA

## Purpose

A userscript for Zendesk views.

This script changes the `Next SLA breach` title in the table heaer to just `SLA` so the table is a bit more compact.\
The SLA color pills in the column fit nicely in the smaller column.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_short_sla/script.user.js

## Warning

Due to how Zendesk updates the tables, this script is not perfect. \
When you switch between Zendesk views you'll see that an incorrect column will have the `SLA` title.
If this bothers you, a page refresh will solve it.

Despite this deficiency I find the script very useful. 

## Technical

Zendesk's table views are... weird. \
A ticket list is shown in two tables. \
The first table has one row with the header. \
The second table has a duplicated header row, and then data rows. \
If you want to modify the header, you'll have to do this for both tables.

## Changelog

- 1.0.0
  - Public release
