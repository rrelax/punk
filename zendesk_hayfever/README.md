# Zendesk Hayfever

## Purpose

A userscript to skin Zendesk with a dark theme.

This summer is one of the worst ever for my hayfever. \
I survive by working in an almost dark room to avoid having my eyes getting more agitated. \
That's ok-ish, except for the fact that I have to work a lot with Zendesk, which sandblasts my eyes.

Time to slap a dark theme on Zendesk.

If you combine this with [Zendesk: SLA colors](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_sla_colors)
you get a thing of beauty:

![](img/with_sla_colors.png)


## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hayfever/script.user.js

## Configuration

No configuration (yet?).

## Technical

I started with the standard trick of applying a `filter: invert` (this is the trick that the instant-darktheme extensions out there use). \
The result left a lot to be desired, so I decided to try to create a dark theme myself.

How hard can it be, eh? \
Well, overruling some of Zendesk's (dynamic) styles proves to be annoying.

My first attempt had a lot covered, but I couldn't restyle the ticket preview popup restyled. \
That was annoying enough to start from scratch again. \
I now start by defining a dark background for `html`, `body`, and `div`, and start tweaking from there.

There are still a lot of details that need fixing, but it's workable.

I might try another approach where I use a modified `<style id="color-branding">` as base.

## Changelog

- 0.0.7
  - Paging: highlight current page
- 0.0.6
  - Hide checkbox
- 0.0.5
  - Fix collission hover (eye icon) styling
- 0.0.4
  - Fix active view title styling
- 0.0.3
  - Only remove branding once
- 0.0.2
  - Initial release
